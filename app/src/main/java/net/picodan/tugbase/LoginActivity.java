package net.picodan.tugbase;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class LoginActivity extends AppCompatActivity {

    public static final String LOGIN_MESSAGE = "LOGIN_MESSAGE";

    private EditText usernameTextBox;

    private Button loginButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        usernameTextBox = (EditText) findViewById(R.id.login_username);

        loginButton = (Button) findViewById(R.id.login_submit);

    }

    public void launchNextScreen(View view) {

        Intent intent = new Intent(this, LocationActivity.class);

        intent.putExtra(LOGIN_MESSAGE, usernameTextBox.getText().toString());

        startActivity(intent);

    }

}
