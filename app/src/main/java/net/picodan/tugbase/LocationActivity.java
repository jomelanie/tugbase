package net.picodan.tugbase;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

import static net.picodan.tugbase.LoginActivity.LOGIN_MESSAGE;

public class LocationActivity extends AppCompatActivity {

    TextView locationText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_location);

        Intent intent = getIntent();
        String loginMessage = intent.hasExtra(LOGIN_MESSAGE) ? intent.getStringExtra(LOGIN_MESSAGE): "No message";

        locationText = (TextView) findViewById(R.id.location_text);
        locationText.setText(loginMessage);


    }
}
